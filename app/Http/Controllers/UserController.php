<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use App\Events\UserWasRegistered;

use Mail;
use App\Mail\RegistrationEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::paginate(20);
        return view("user.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("user.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = [
            'email' => 'required|email|unique:App\Models\City,cod',
            'name' => 'required|string|max:100'
        ];
        $messages = [
            'email.required' => 'El correo electrónico es requerido',
            'email.unique' => 'El correo electrónico ya ha sido asignado a otro usuario',
            'name.required' => 'El nombre es requerido',
        ];

        $this->validate($request, $fields, $messages);

        $receivedData = $request->except('_token');
        if($request->hasFile('photo'))
        {
            $receivedData['photo'] = $request->file('photo')->store('uploads', 'public');
        }
        $receivedData['password'] = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user = User::create($receivedData);
        $this->sendMail($user);
        //UserWasRegistered::dispatch($receivedData['email']);

        return redirect("users")->with('user-message', 'Usuario agregado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $user = User::findOrFail($id);
        return view("user.edit", compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = [
            'name' => 'required|string|max:100'
        ];
        $messages = [
            'name.required' => 'El nombre es requerido',
        ];

        $this->validate($request, $fields, $messages);

        $user = User::findOrFail($id);
        if($request->hasFile('photo'))
        {
            Storage::delete('public/' . $user->photo );
            $receivedData['photo'] = $request->file('photo')->store('uploads', 'public');
        }


        $receivedData = $request->except(['_token', '_method']);
        User::where('id', '=', $id)->update($receivedData);
        return redirect("users")->with('user-message', 'Usuario modificado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        Storage::delete('public/' . $user->photo );
        User::destroy($id);
        return redirect('users')->with('user-message', 'Usuario eliminado exitosamente');;
    }

    public function resendMail($id)
    {
        $user = User::findOrFail($id);
        $this->sendMail($user);
        return redirect('users')->with('user-message', 'Correo enviado');;
    }

    private function sendMail(User $user)
    {
        //$email = new RegistrationEmail($user->id, $this->getResetLink($user->id));
        //Mail::to($user->email)->send($email);
        UserWasRegistered::dispatch($user,  $this->getResetLink($user->id));
    }

    public function getResetLink($id)
    {
        return URL::temporarySignedRoute(
            'user.showResetForm',
            now()->addMinutes(60),
            ['id' => $id]
        );
    }

    public function showResetPasswordForm(Request $request, $id)
    {
        /*
        if (! $request->hasValidSignature()) {
            abort(403);
        }
        */
        return view('user.reset', ['id' => $id]);
    }

    public function resetPassword(Request $request, $id)
    {
        /*
        if (! $request->hasValidSignature()) {
            abort(403);
        }
        */
        $validator = Validator::make($request->all(), []);
        $validator->sometimes(['password', 'password_confirm'], 'required', function ($input) {
            return $input->password === $input->password_confirm;
        });
        $validator->validate();
        $user = User::findOrFail($id);
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect("login");
    }


}
