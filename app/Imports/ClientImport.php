<?php

namespace App\Imports;

use App\Models\City;
use App\Models\Client;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithUpserts;

class ClientImport implements ToModel, WithHeadingRow, WithChunkReading, WithBatchInserts, WithUpserts
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $newClient = new Client([
            'cod' => $row['cod'],
            'name' => $row['name'],
        ]);

        $codCity = $row["cod_city"];

        $foundity = City::where('cod', $codCity)->first();

        $newClient->city()->associate($foundity);
        $newClient->save();
        return $newClient;
    }

    public function chunkSize(): int
    {
        return 10000;
    }

    public function batchSize(): int
    {
        return 10000;
    }

    public function uniqueBy()
    {
        return 'cod';
    }
}
