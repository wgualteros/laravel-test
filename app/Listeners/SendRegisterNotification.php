<?php

namespace App\Listeners;

use App\Events\UserWasRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Jobs\SendEmailJob;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class SendRegisterNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasRegistered  $event
     * @return void
     */
    public function handle(UserWasRegistered $event)
    {
        $details['email'] = $event->getUser()->email;
        $details['userId'] = $event->getUser()->id;
        $details['link'] = $event->getLnk();
        dispatch(new SendEmailJob($details));
    }
}
