<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $userId;
    private $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userId, $link)
    {
        $this->userId = $userId;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('user.registration', ['userId' => $this->userId, 'link' => $this->link]);
    }
}
