<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_clients';

    protected $fillable = [
        'cod',
        'name',
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'id_city');
    }
}
