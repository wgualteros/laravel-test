@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{url("/cities")}}" method="post">
            @csrf
            @include('city.form', ['mode' => 'create'])
        </form>
    </div>
@endsection
