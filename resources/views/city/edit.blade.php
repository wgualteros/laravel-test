@extends('layouts.app')

@section('content')
    <div class="container">

        <form action="{{url('cities/' . $city->id )}}" method="post">
            @csrf
            {{ method_field('PATCH')}}
            @include('city.form', ['mode' => 'edit'])
        </form>
    </div>
@endsection
