
@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Session::has("user-message"))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ Session::get("user-message") }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <a href="{{url('cities/create')}}" class="btn btn-success mb-3">Crear ciudad</a>
        <a href="{{url('cities/import')}}" class="btn btn-info mb-3">Importar</a>
        <a href="{{url('cities/export')}}" class="btn btn-primary mb-3">Exportar</a>

        {{ $cities->links() }}
        <table class="table table-bordered table-danger table-hover table-active text-center">
            <thead class="thead-dark">
                <tr>
                    <th>Id</th>
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cities as $city)
                <tr>
                    <th scope="row" class="align-middle">{{$city->id}}</th>
                    <td class="align-middle">{{$city->cod}}</td>
                    <td class="align-middle">{{$city->name}}</td>
                    <td>
                        <a
                            href="{{url('cities/' . $city->id . '/edit')}}"
                            class="btn btn-info"
                        >
                            Editar
                        </a>
                        @if($city->clients->count() == 0)

                            <form
                                action="{{url('cities/' . $city->id)}}"
                                method="post"
                                class="d-inline"
                            >
                            @csrf
                            {{ method_field('DELETE')}}
                            <input
                                class="btn btn-danger"
                                type="submit"
                                value="Elminar"
                                onclick="return confirm('¿Esá seguro de que quiere eliminar la ciudad?');"
                            >
                            </form>
                        @endif($city->clients())
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $cities->links() }}
    </div>
@endsection
