@if($mode=='edit')
    <h2>Editar Usuario</h2>
@elseif ($mode=='create')
    <h2>Crear Usuario</h2>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
        @foreach($errors->all() as $error)
            <li>
            {{$error}}
            </li>
        @endforeach
        </ul>
    </div>
@endif




<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{isset($user->name)?$user->name:old('name')}}" required autocomplete="name" autofocus>
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electrónico') }}</label>

    <div class="col-md-6">
        @if($mode=='edit')
            <input id="email" type="email" readonly class="form-control @error('email') is-invalid @enderror" name="email" value="{{ isset($user->email)?$user->email:old('email') }}" required autocomplete="email">
        @elseif ($mode=='create')
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ isset($user->email)?$user->email:old('email') }}" required autocomplete="email">
        @endif
    </div>
</div>

<div class="form-group row">
    @if(isset($user->photo))
        <img class="img-thumbnail img-fluid" src="{{ asset('storage') . '/' . $user->photo}}" alt="user 
        avatar">
    @endif
    <div class="col-md-6">
        <input id="photo" type="file" class="form-control @error('photo') is-invalid @enderror" name="photo" value="{{isset($user->photo)?$user->photo:old('photo')}}" autofocus>
    </div>
</div>


@if($mode=='edit')
    <input type="submit" value="Guardar Cambios" class="btn btn-success">
@elseif ($mode=='create')
    <input type="submit" value="Crear Usuario" class="btn btn-success">
@endif
<a href="{{url('users')}}" class="btn btn-primary">Regresar</a>
