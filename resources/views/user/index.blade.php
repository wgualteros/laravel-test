@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Session::has("user-message"))
            <div class="alert alert-success alert-dismissible" role="alert">
                {{ Session::get("user-message") }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <h2 class="text-center">Lista de Usuarios</h2>
        <div class="row">
            <a href="{{url('users/create')}}" class="btn btn-success mb-3">Crear usuario</a>
        </div>
        {{ $users->links() }}
        <table class="table table-bordered table-danger table-hover table-active text-center">
            <thead  class="thead-dark">
                <tr>
                    <th>Id</th>
                    <th>Correo Electrónico</th>
                    <th>Nombre</th>
                    <th>Foto</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <th scope="row" class="align-middle">{{$user->id}}</th>
                    <td class="align-middle">{{$user->email}}</td>
                    <td class="align-middle">{{$user->name}}</td>
                    <td class="align-middle">
                        @if($user->photo)
                            <img class="img-thumbnail img-fluid" src="{{ asset('storage') . '/' . $user->photo}}" alt="user
                            avatar">
                        @endif
                    </td>
                    <td>
                        <a href="{{url('users/' . $user->id . '/edit')}}" class="btn btn-info">Editar</a>
                        @if(Auth::user()->email !== $user->email)
                            <form
                                action="{{url('users/' . $user->id . '/sendMail')}}"
                                method="post"
                                class="d-inline"
                            >
                                @csrf
                                <input
                                    class="btn btn-warning"
                                    type="submit"
                                    value="Enviar Correo"
                                >
                            </form>
                            <form
                                action="{{url('users/' . $user->id)}}"
                                method="post"
                                class="d-inline"
                            >
                            @csrf
                            {{ method_field('DELETE')}}
                            <input
                                class="btn btn-danger"
                                type="submit"
                                value="Elminar"
                                onclick="return confirm('¿Esá seguro de que quiere eliminar el usuario?');"
                            >
                            </form>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
@endsection
