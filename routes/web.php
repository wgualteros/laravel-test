<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\CityController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function() {
    Route::get('cities/import', [ImportController::class, 'importCitiesForm']);
    Route::post('cities/import', [ImportController::class, 'importCities']);
    Route::get('cities/export', [ExportController::class, 'exportCities']);
    Route::resource('cities', CityController::class);

    Route::get('clients/import', [ImportController::class, 'importClientsForm']);
    Route::post('clients/import', [ImportController::class, 'importClients']);
    Route::get('clients/export', [ExportController::class, 'exportClients']);
    Route::resource('clients', ClientController::class);


    Route::get('users/{id}/resetPassword', [UserController::class, 'showResetPasswordForm'])->name('user.showResetForm')->middleware('signed');

    Route::post('users/{id}/resetPassword', [UserController::class, 'resetPassword'])->name('user.showResetForm')->name('user.resetPassword');

    Route::resource('users', UserController::class);
    Route::post('users/{id}/sendMail', [UserController::class, 'resendMail']);

});



Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['register' => false]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => 'auth'], function() {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
});

